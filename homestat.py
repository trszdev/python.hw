#!/usr/bin/env python3
import re
import itertools


def make_stat(filename):
    """
    Функция вычисляет статистику по именам за каждый год с учётом пола.
    """
    with open(filename, 'r', encoding='cp1251') as fhandle:
        text = fhandle.read()
        result = {}
        table_contents = re.search(r'<table.*?>(.+)</table>', text, re.S)

        cur_year = ''
        cur_students = []
        if table_contents:
            for line in table_contents.group(1).split(r'<tr>'):
                year = re.search(r'>(\d{4})<', line)
                student = re.search(r'<a href=(.+?)>(.+)</a>', line)
                if year:
                    if cur_year:
                        result[cur_year] = cur_students
                        cur_students = []
                    cur_year = year.group(1)
                if student:
                    cur_students.append(student.group(2).split()[1])
            if cur_year:
                result[cur_year] = cur_students
        return result


def is_male(name):
    return any(x in name.lower() for x in [
        'дмитрий', 'евгений', 'александр', 'георгий', 'павел',
        'антон', 'сергей', 'булат', 'константин', 'ростислав',
        'андрей', 'илья', 'алехандро', 'лёва', 'роман', 'никита',
        'кирилл', 'игорь'
    ])


def is_female(name):
    return not is_male(name)


def is_any(x): return True


def extract_tuples(names, name_filter):
    result = {}
    for name in filter(name_filter, names):
        if name in result:
            result[name] += 1
        else:
            result[name] = 1
    return sorted(result.items(), key=lambda x: x[1], reverse=True)


def extract_years(stat):
    """
    Функция принимает на вход вычисленную статистику и выдаёт список годов,
    упорядоченный по возрастанию.
    """
    return list(sorted(stat))


def extract_general(stat):
    """
    Функция принимает на вход вычисленную статистику и выдаёт список tuple'ов
    (имя, количество) общей статистики для всех имён.
    Список должен быть отсортирован по убыванию количества.
    """
    all_names = itertools.chain(*stat.values())
    return extract_tuples(all_names, is_any)


def extract_general_male(stat):
    """
    Функция принимает на вход вычисленную статистику и выдаёт список tuple'ов
    (имя, количество) общей статистики для имён мальчиков.
    Список должен быть отсортирован по убыванию количества.
    """
    all_names = itertools.chain(*stat.values())
    return extract_tuples(all_names, is_male)


def extract_general_female(stat):
    """
    Функция принимает на вход вычисленную статистику и выдаёт список tuple'ов
    (имя, количество) общей статистики для имён девочек.
    Список должен быть отсортирован по убыванию количества.
    """
    all_names = itertools.chain(*stat.values())
    return extract_tuples(all_names, is_female)


def extract_year(stat, year):
    """
    Функция принимает на вход вычисленную статистику и год.
    Результат — список tuple'ов (имя, количество) общей статистики для всех
    имён в указанном году.
    Список должен быть отсортирован по убыванию количества.
    """
    return extract_tuples(stat[year], is_any)


def extract_year_male(stat, year):
    """
    Функция принимает на вход вычисленную статистику и год.
    Результат — список tuple'ов (имя, количество) общей статистики для всех
    имён мальчиков в указанном году.
    Список должен быть отсортирован по убыванию количества.
    """
    return extract_tuples(stat[year], is_male)


def extract_year_female(stat, year):
    """
    Функция принимает на вход вычисленную статистику и год.
    Результат — список tuple'ов (имя, количество) общей статистики для всех
    имён девочек в указанном году.
    Список должен быть отсортирован по убыванию количества.
    """
    return extract_tuples(stat[year], is_female)


if __name__ == '__main__':
    print(make_stat('home.html'))

