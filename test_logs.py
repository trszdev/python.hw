import logs, unittest

class Writer:
    def __init__(self):
        self.buffer = ''

    def write(self, x):
        self.buffer += x


class LogTestBase:
    def __init__(self, stats, answers_name):
            self.got = stats
            with open(answers_name, 'r') as answers:
                self.expected = {}
                self.answers_content = answers.read()
                for line in self.answers_content.split('\n'):
                    if len(line.split(':')) != 2: continue
                    field, value = line.split(':')
                    value = value[1:]
                    if field == 'MostActiveClientByDay':
                        self.expected[field] = []
                    elif field.startswith(' ' * 2):
                        self.expected['MostActiveClientByDay'].append(line.strip())
                    else:
                        self.expected[field] = value

    def test_file_format(self):
        writer = Writer()
        logs.print_stats(self.got, writer)
        self.assertEquals(writer.buffer, self.answers_content)

    def assertFieldEquals(self, field_name):
        self.assertEquals(self.got[field_name].get_value(), self.expected[field_name])
    
    def test_field_FastestPage(self):
        self.assertFieldEquals('FastestPage')
        
    def test_field_MostActiveClient(self):
        self.assertFieldEquals('MostActiveClient')

    def test_field_MostActiveClientByDay(self):
        got_field_value = self.got['MostActiveClientByDay'].get_value()
        got_field_value = filter(bool, got_field_value.split('\n'))
        got_field_value = [x.strip() for x in got_field_value]
        self.assertEquals(got_field_value, self.expected['MostActiveClientByDay'])

    def test_field_MostPopularPage(self):
        self.assertFieldEquals('MostPopularPage')

    def test_field_MostPopularBrowser(self):
        self.assertFieldEquals('MostPopularBrowser')

    def test_field_SlowestPage(self):
        self.assertFieldEquals('SlowestPage')

    def test_field_SlowestAveragePage(self):
        self.assertFieldEquals('SlowestAveragePage')


def create_log_test(test_name, log_name, answers_name):
    with open(log_name, 'r') as log:
        stats = logs.get_stats(log)

        def init(self, method_name):
            LogTestBase.__init__(self, stats, answers_name)
            unittest.TestCase.__init__(self, method_name)

        globals()[test_name] = \
            type(test_name, (unittest.TestCase, LogTestBase), {'__init__': init})


# Dynamically create testcases
create_log_test('FirstLog', '1.log', '1_out.txt')

create_log_test('SecondLog', 'example_2.log', 'example_out_2.txt')
        
if __name__ == '__main__':
    unittest.main()
