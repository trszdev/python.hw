#!/usr/bin/env python3
import sys
import re
from collections import OrderedDict
from datetime import datetime


class StatsField:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def get_value(self):
        if hasattr(self, 'value'):
            return self.value


class ComparatorField(StatsField):
    def __init__(self, *args):
        StatsField.__init__(self)
        self.key_name, self.value_name, self.compare = args

    def update(self, stats):
        key, value = stats[self.key_name], stats[self.value_name]
        if not hasattr(self, 'key'):
            self.key = key
            self.value = value
        else:
            comparison = self.compare(self.key, key)
            if comparison > 0:
                self.key = key
                self.value = value
            elif comparison == 0:
                self.value = value


class MostFrequentField(StatsField):
    def __init__(self, *args):
        StatsField.__init__(self)
        self.field_name, = args
        self.frequencies = {}

    def update(self, stats):
        value = stats[self.field_name]
        if value in self.frequencies:
            self.frequencies[value] += 1
        else:
            self.frequencies[value] = 1

    def get_value(self):
        max_freq = max(self.frequencies.values())
        candidates = [k for k, v in
                      self.frequencies.items() if v == max_freq]
        return min(candidates)


class MostFrequentByDayField(StatsField):
    def __init__(self, *args):
        StatsField.__init__(self)
        self.field_name, = args
        self.days = {}

    def update(self, stats):
        day = stats['timestamp'].date()
        if day not in self.days:
            self.days[day] = MostFrequentField(self.field_name)
        self.days[day].update(stats)

    def get_value(self):
        result = ['']
        for day in sorted(self.days):
            field = self.days[day]
            to_append = '{}: {}'.format(day.isoformat(), field.get_value())
            result.append(to_append)
        return '\n  '.join(result) + '\n'


def parse_line(line):
    regex = re.compile(r'^([\d\.]+) - - \[(.+?)\] '
                       r'"([A-Z]+) (.+?) (HTTP/1.1|HTTP/1.0)" '
                       r'(\d+) (\d+) "(.+?)" "(.+?)" ?(\d+)?$')
    keys = ('ip', 'timestamp', 'method', 'url', 'version', 'code',
            'size', 'referrer', 'user_agent', 'load_time')
    values = regex.findall(line)[0]
    result = dict(zip(keys, values))
    field_format = {
        'timestamp': lambda x: datetime.strptime(x, '%d/%b/%Y:%H:%M:%S %z'),
        'code': int,
        'size': int,
        'load_time': int,
    }
    for name, format in field_format.items():
        if name in result:
            result[name] = format(result[name])
    return result


def get_stats(input_file):
    def max_compare(x, y):
        return 1 if x < y else (-1 if x > y else 0)

    def min_compare(x, y):
        return -1 * max_compare(x, y)

    stats = OrderedDict()
    stats['FastestPage'] = ComparatorField('load_time', 'url', min_compare)
    stats['MostActiveClient'] = MostFrequentField('ip')
    stats['MostActiveClientByDay'] = MostFrequentByDayField('ip')
    stats['MostPopularBrowser'] = MostFrequentField('user_agent')
    stats['MostPopularPage'] = MostFrequentField('url')
    stats['SlowestAveragePage'] = \
        ComparatorField('load_time', 'url', max_compare)
    stats['SlowestPage'] = ComparatorField('load_time', 'url', max_compare)

    for parsed in map(parse_line, input_file):
            for field in stats.values():
                try:
                    field.update(parsed)
                except KeyError:
                    pass
    return stats


def print_stats(stats, out_file):
    for (name, field) in stats.items():
        out_file.write('{}: {}\n'.format(name, field.get_value()))
    out_file.write('\n')


if __name__ == '__main__':
    stats = get_stats(sys.stdin)
    print_stats(stats, sys.stdout)
