#!/usr/bin/env python3
import urllib.request
import urllib.parse
import re
import heapq
import sys


_precompute_max_depth = 1
_precompute = {}


def precompute(finish_name):
    if finish_name in _precompute:
        return _precompute[finish_name]
    q = [(finish_name, 0)]
    pre = {finish_name: [finish_name]}
    visited = set()
    while q:
        name, depth = q.pop(0)
        if depth >= _precompute_max_depth:
            continue
        visited.add(name)
        for i, adj in enumerate(get_assoc(name)):
            if adj in visited:
                continue
            q.append((adj, depth+1))
            pre[adj] = [adj] + pre[name]
    _precompute[finish_name] = pre
    return pre


def get_assoc(name):
    content = get_content(name)
    bounds = extract_content(content)
    assoc = extract_links(content, *bounds)
    return assoc


def wiki_link(name):
    return 'http://ru.wikipedia.org/wiki/{}'\
        .format(urllib.parse.quote_plus(name))


def get_content(name):
    """
    Функция возвращает содержимое вики-страницы name из русской Википедии.
    В случае ошибки загрузки или отсутствия страницы возвращается None.
    """
    link = wiki_link(name)
    try:
        resource = urllib.request.urlopen(link)
        encoding = resource.headers.get_content_charset()
        content = resource.read().decode(encoding)\
            .encode('cp866', 'ignore').decode('cp866')
        if 'нет статьи' in content:
            raise Error('No such page "{}"'.format(name))
        return content
    except:
        return None


def extract_content(page):
    """
    Функция принимает на вход содержимое страницы и возвращает 2-элементный
    tuple, первый элемент которого — номер позиции, с которой начинается
    содержимое статьи, второй элемент — номер позиции, на котором заканчивается
    содержимое статьи.
    Если содержимое отсутствует, возвращается (0, 0).
    """
    if not page:
        return 0, 0
    if '<p><b>' in page:
        return page.index('<p><b>'), len(page)
    return 0, len(page)


def extract_links(page, begin, end):
    """
    Функция принимает на вход содержимое страницы и начало и конец интервала,
    задающего позицию содержимого статьи на странице и возвращает все имеющиеся
    ссылки на другие вики-страницы без повторений и с учётом регистра.
    """
    if not page:
        return []
    urls = re.findall(r'''["']/wiki/([^"'#:]+)["']''', page[begin:end])
    urls = map(urllib.parse.unquote, urls)
    uniq = set()
    result = []
    for url in urls:
        if url not in uniq:
            uniq.add(url)
            result.append(url)
    return result


def find_chain(start, finish):
    """
    Функция принимает на вход название начальной и конечной статьи и возвращает
    список переходов, позволяющий добраться из начальной статьи в конечную.
    Первым элементом результата должен быть start, последним — finish.
    Если построить переходы невозможно, возвращается None.
    """
    if start == finish:
        return [start, finish]
    visited = set()
    track = {start: ''}
    q = [(1, start)]
    pre = precompute(finish)
    while q:
        _, name = heapq.heappop(q)
        if name in visited:
            continue
        visited.add(name)
        assoc = get_assoc(name)
        for i, next in enumerate(assoc):
            if next not in visited:
                if next not in pre:
                    heapq.heappush(q, (i, next))
                else:
                    heapq.heappush(q, (-len(pre[next]), next))
                track[next] = name
                if next == finish:
                    path = []
                    last = next
                    while last:
                        path.append(last)
                        last = track[last]
                    path.reverse()
                    return path


def main(name):
    chain = find_chain(name, 'Философия')
    if chain:
        print(chain)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(sys.argv[1])

